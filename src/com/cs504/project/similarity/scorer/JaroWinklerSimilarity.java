package com.cs504.project.similarity.scorer;

import com.wcohen.ss.JaroWinkler;

public class JaroWinklerSimilarity implements SimilarityScorer {

	@Override
	public double score(String s1, String s2) {
		JaroWinkler jaroWinkler = new JaroWinkler();
		return jaroWinkler.score(jaroWinkler.prepare(s1), jaroWinkler.prepare(s2));
	}

}
