package com.cs504.project.similarity.scorer;

import com.wcohen.ss.JaroWinklerTFIDF;

public class JaroWinklerTFIDFSimilarity implements SimilarityScorer {

	@Override
	public double score(String s1, String s2) {
		JaroWinklerTFIDF jaroWinklerTfidf = new JaroWinklerTFIDF();
		return jaroWinklerTfidf.score(jaroWinklerTfidf.prepare(s1), jaroWinklerTfidf.prepare(s2));
	}

}
