package com.cs504.project.similarity.scorer;

import com.wcohen.ss.TFIDF;

public class TFIDFSimilarity implements SimilarityScorer {

	@Override
	public double score(String s1, String s2) {
		TFIDF tfidf = new TFIDF();
		return tfidf.score(tfidf.prepare(s1), tfidf.prepare(s2));
	}
}
