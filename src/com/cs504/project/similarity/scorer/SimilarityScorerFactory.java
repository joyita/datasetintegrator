package com.cs504.project.similarity.scorer;


public class SimilarityScorerFactory {
	public static SimilarityScorer getSimilarityScorer(String scoringType) {
		if (scoringType.equalsIgnoreCase(ScoringType.SCORE_JARO)) {
			return new JaroSimilarity();
		} 
		else if (scoringType.equalsIgnoreCase(ScoringType.SCORE_JARO_WINKLER)) {
			return new JaroWinklerSimilarity();
		}
		else if (scoringType.equalsIgnoreCase(ScoringType.SCORE_JACCARD)) {
			return new JaccardSimilarity();
		}
		else if (scoringType.equalsIgnoreCase(ScoringType.SCORE_TFIDF)) {
			return new TFIDFSimilarity();
		}
		else if (scoringType.equalsIgnoreCase(ScoringType.SCORE_JARO_WINKLER_TFIDF)) {
			return new JaroWinklerTFIDFSimilarity();
		}
		
		return null;
	}
}
