package com.cs504.project.similarity.scorer;

import com.wcohen.ss.Jaro;

public class JaroSimilarity implements SimilarityScorer {

	@Override
	public double score(String s1, String s2) {
		Jaro jaro = new Jaro();
		return jaro.score(jaro.prepare(s1), jaro.prepare(s2));
	}

}
