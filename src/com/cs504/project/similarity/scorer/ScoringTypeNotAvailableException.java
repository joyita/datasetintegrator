package com.cs504.project.similarity.scorer;

@SuppressWarnings("serial")
public class ScoringTypeNotAvailableException extends Exception {
	public ScoringTypeNotAvailableException() {
		super();
	}
	
	public ScoringTypeNotAvailableException(String message) {
		super(message);
	}
}
