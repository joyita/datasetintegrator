package com.cs504.project.similarity.scorer;

public interface SimilarityScorer {
	public double score(String s1, String s2);
}
