package com.cs504.project.similarity.scorer;

import com.wcohen.ss.Jaccard;

public class JaccardSimilarity implements SimilarityScorer {

	@Override
	public double score(String s1, String s2) {
		Jaccard jaccard = new Jaccard();
		return jaccard.score(jaccard.prepare(s1), jaccard.prepare(s2));
	}

}
