package com.cs504.project.similarity.scorer;

public class ScoringType {
	public static final String SCORE_EMPTY = "None";
	public static final String SCORE_JARO = "Jaro";
	public static final String SCORE_JARO_WINKLER = "JaroWinkler";
	public static final String SCORE_JACCARD = "Jaccard";
	public static final String SCORE_TFIDF = "TFIDF";
	public static final String SCORE_JARO_WINKLER_TFIDF = "JaroWinklerTFIDF";
}
