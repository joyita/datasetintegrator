package com.cs504.project.ui;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class DatasetIntegratorUI extends JPanel { 
    private JTable table;

    public DatasetIntegratorUI(String inputFilePath, final String outputFilePath) {
    	super();
    	setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    	
        table = new JTable(new MyTableModel(inputFilePath));
        table.setPreferredScrollableViewportSize(new Dimension(500, 500));
        table.setFillsViewportHeight(true);
        table.setRowSelectionAllowed(true);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table.setAutoCreateRowSorter(true);
        add(new JScrollPane(table));
        
        JButton button = new JButton("Save Matched Entities");
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				createTextFile(outputFilePath);
			}
		});
        add(button);
    }
    
    private void createTextFile(String filePath) {
    	String text = "";
    	for (int i = 0; i < table.getColumnCount() - 1; i++) {
    		text += table.getColumnName(i) + " ";
    	}
    	text += "\n";
    	for (int i = 0; i < table.getRowCount(); i++) {
    		if ((boolean) table.getValueAt(i, table.getColumnCount() - 1)) {
    			for (int j = 0; j < table.getColumnCount() - 1; j++) {
        			text += "\"" + table.getValueAt(i, j) + "\" ";
        		}
    			text += "\n";
    		}
    	}
    	
    	PrintWriter file = null;
		try {
			file = new PrintWriter(filePath);
			file.write(text);
			System.out.println("Created text file: " + filePath);
		} catch (FileNotFoundException e) {
			System.err.println("Error creating file: " + filePath);
		} finally {
			if (file != null) {
				file.close();
			}
		}
    }

    class MyTableModel extends AbstractTableModel {
        private String[] columnNames;
        private Object[][] data;
        
        public MyTableModel(String filePath) {
        	try {
				InputStream in = new FileInputStream(new File(filePath));
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				String line;
				if ((line = reader.readLine()) != null) {
					String[] tokens = line.split(" ");
					columnNames = new String[tokens.length + 1];
					for (int i = 0; i < tokens.length; i++) {
						columnNames[i] = tokens[i];
					}
					columnNames[tokens.length] = "Matched";
				}

				List<String> lineValues = new ArrayList<String>();
		        while ((line = reader.readLine()) != null) {
		        	lineValues.add(line);
		        }
		        
		        data = new Object[lineValues.size()][columnNames.length];
		        for (int i = 0; i < lineValues.size(); i++) {
		        	String[] tokens = lineValues.get(i).split("\" \"");
		        	for (int j = 0; j < tokens.length; j++) {
						data[i][j] = tokens[j].replace("\"", "");
					}
		        	data[i][tokens.length] = new Boolean(false);
		        }
		        
		        reader.close();
			} 
        	catch (FileNotFoundException e) {
				e.printStackTrace();
			} 
        	catch (IOException e) {
				e.printStackTrace();
			}       	
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        /*
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col < 2) {
                return false;
            } else {
                return true;
            }
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            data[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static void createAndShowGUI(String inputFilePath, String outputFilePath) {
        //Disable boldface controls.
        UIManager.put("swing.boldMetal", Boolean.FALSE); 

        //Create and set up the window.
        JFrame frame = new JFrame("DataIntegratorUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        DatasetIntegratorUI newContentPane = new DatasetIntegratorUI(inputFilePath, outputFilePath);
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
}
