package com.cs504.project.testrunners;

import java.sql.SQLException;
import java.util.List;

import com.cs504.project.dataset.GLEIEntity;
import com.cs504.project.dataset.matcher.EntityMatcher;
import com.cs504.project.similarity.scorer.ScoringType;
import com.cs504.project.similarity.scorer.ScoringTypeNotAvailableException;


public class EntityMatcherTestRunner {
	public static void main(String args[]) {
		String url = "jdbc:mysql://localhost:3306/cs504project";
		String user = "root";
		String password = "rootpass";
		String gleiCountryTable = "cs504project.gleius";
		String offshoreEntityTable = "cs504project.offshoreentity";
		
		EntityMatcher entityMatcher;
		try {
			entityMatcher = new EntityMatcher();
			entityMatcher.connectToDatabase(url, user, password);
			entityMatcher.createQueryMaker(gleiCountryTable, offshoreEntityTable);
			entityMatcher.match(ScoringType.SCORE_TFIDF, 0.6);
			List<GLEIEntity> matchedGLEIEntities = entityMatcher.getMatchedGLEIEntities();		
			System.out.println("Matched GLEI entities: " + matchedGLEIEntities.size());
			entityMatcher.generateTextFileForMatchedEntities("/Users/mahzabin/Documents/workspace/DatasetIntegrator/output/MatchedEntities-TFIDF.txt");
			entityMatcher.closeConnector();
		} catch (ScoringTypeNotAvailableException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
}
