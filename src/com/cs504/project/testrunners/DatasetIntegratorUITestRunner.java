package com.cs504.project.testrunners;

import com.cs504.project.ui.DatasetIntegratorUI;

public class DatasetIntegratorUITestRunner {
	public static void main(String[] args) {
    	final String inputFilePath = "/Users/mahzabin/Documents/workspace/DatasetIntegrator/output/MatchedEntities-Jaccard.txt";
    	final String outputFilePath = "/Users/mahzabin/Documents/workspace/DatasetIntegrator/output/NewMatchedEntities-Jaccard.txt";
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                DatasetIntegratorUI.createAndShowGUI(inputFilePath, outputFilePath);
            }
        });
    }
}
