package com.cs504.project.dataset;

public class OffshoreEntity {
	private String uniqueID;
	private String description;
	private String address;
	private double similarityScoreForName;
	private double similarityScoreForAddr;
	
	public OffshoreEntity(String uniqueID, String description, String address) {
		this.uniqueID = uniqueID;
		this.description = description;
		this.address = address;
	}
	
	public String getUniqueID() {
		return uniqueID;
	}
	
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public double getSimilarityScoreForName() {
		return similarityScoreForName;
	}

	public void setSimilarityScoreForName(double similarityScoreForName) {
		this.similarityScoreForName = similarityScoreForName;
	}

	public double getSimilarityScoreForAddr() {
		return similarityScoreForAddr;
	}

	public void setSimilarityScoreForAddr(double similarityScoreForAddr) {
		this.similarityScoreForAddr = similarityScoreForAddr;
	}
}
