package com.cs504.project.dataset;

public class TableAttributes {
	public static final String OFFSHORE_ATTR_UNIQUE_ID = "UniqueID";
	public static final String OFFSHORE_ATTR_DESCRIPTION = "Description";
	public static final String OFFSHORE_ATTR_ADDRESS = "CompleteAddresses";
	
	public static final String GLEI_ATTR_LEI = "LEI";
	public static final String GLEI_ATTR_LEGAL_NAME = "LegalName";
	public static final String GLEI_ATTR_CITY = "City";
	public static final String GLEI_ATTR_ADDRESS = "Address";
}
