package com.cs504.project.dataset.matcher;

import com.cs504.project.dataset.TableAttributes;

public class QueryMaker {
	private String gleiCountryTable;
	private String offshoreEntityTable;
	
	public QueryMaker(String GLEICountryTable, String offshoreEntityTable) {
		this.gleiCountryTable = GLEICountryTable;
		this.offshoreEntityTable = offshoreEntityTable;
	}
	
	public String getAllGLEICountryEntities() {
		return "SELECT " + TableAttributes.GLEI_ATTR_LEI + ", " + 
				TableAttributes.GLEI_ATTR_LEGAL_NAME + ", " + 
				TableAttributes.GLEI_ATTR_CITY + ", " + 
				TableAttributes.GLEI_ATTR_ADDRESS + 
			   " FROM " + gleiCountryTable;
	}
	
	public String getFirstWordMatchedOffshoreEntities(String firstWord) {
		return "SELECT " + TableAttributes.OFFSHORE_ATTR_UNIQUE_ID + ", " + 
			    TableAttributes.OFFSHORE_ATTR_DESCRIPTION + ", " + 
			    TableAttributes.OFFSHORE_ATTR_ADDRESS + 
			   " FROM " + offshoreEntityTable + 
			   " WHERE " + TableAttributes.OFFSHORE_ATTR_DESCRIPTION + " LIKE " + "'" + firstWord.replaceAll("'", "\\\\'") + " %'";
	}
}
