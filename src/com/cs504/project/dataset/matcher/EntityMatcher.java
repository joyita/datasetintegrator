package com.cs504.project.dataset.matcher;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.cs504.project.dataset.GLEIEntity;
import com.cs504.project.dataset.OffshoreEntity;
import com.cs504.project.dataset.TableAttributes;
import com.cs504.project.similarity.scorer.ScoringType;
import com.cs504.project.similarity.scorer.ScoringTypeNotAvailableException;
import com.cs504.project.similarity.scorer.SimilarityScorer;
import com.cs504.project.similarity.scorer.SimilarityScorerFactory;

public class EntityMatcher {
	private DatabaseConnector connector = null;
	private QueryMaker queryMaker = null;
	
	private SimilarityScorer scorerForName;
	private SimilarityScorer scorerForAddr;	
	private boolean matchAddress;

	private List<GLEIEntity> gleiEntities = new ArrayList<GLEIEntity>();
	private List<GLEIEntity> matchedGLEIEntities = new ArrayList<GLEIEntity>();
	
	public void connectToDatabase(String databaseUrl, String databaseUser, String databasePass) 
					throws ClassNotFoundException, SQLException {
		this.connector = new DatabaseConnector(databaseUrl, databaseUser, databasePass);
	}
	
	public void createQueryMaker(String gleiCountryTable, String offshoreCountryTable) {
		this.queryMaker = new QueryMaker(gleiCountryTable, offshoreCountryTable);
	}
	
	public void match (String similarityScoringTypeForName, double similarityThresholdForName) 
			throws ScoringTypeNotAvailableException {
		match(similarityScoringTypeForName, similarityThresholdForName, 
			  ScoringType.SCORE_EMPTY, 0.0);
	}

	public void match(String similarityScoringTypeForName, double similarityThresholdForName,
					  String similarityScoringTypeForAddr, double similarityThresholdForAddr) 
							  throws ScoringTypeNotAvailableException {
		if (connector == null | queryMaker == null) {
			throw new NullPointerException("Connector or QueryMaker cannot be null.");
		}
		
		this.scorerForName = SimilarityScorerFactory.getSimilarityScorer(similarityScoringTypeForName);
		if (this.scorerForName == null) {
			throw new ScoringTypeNotAvailableException("Scoring type is not available for name.");
		}
		
		if (similarityScoringTypeForAddr.equalsIgnoreCase(ScoringType.SCORE_EMPTY) && similarityThresholdForAddr == 0.0) {
			this.matchAddress = false;
		}
		else {
			this.matchAddress = true;
			this.scorerForAddr = SimilarityScorerFactory.getSimilarityScorer(similarityScoringTypeForAddr);
			if (this.scorerForAddr == null) {
				throw new ScoringTypeNotAvailableException("Scoring type is not available for address.");
			}
		}
		
		setGLEIEntities();

		for (GLEIEntity gleiEntity : gleiEntities) {
			String query = queryMaker.getFirstWordMatchedOffshoreEntities(gleiEntity.getFirstWordOfLegalName());
			ResultSet rs = connector.executeQuery(query);
			
			List<OffshoreEntity> offshoreEntities = new ArrayList<OffshoreEntity>();
			
			try {
				while (rs.next()) {
					String uniqueIDValue = rs.getString(TableAttributes.OFFSHORE_ATTR_UNIQUE_ID);
					String descriptionValue = rs.getString(TableAttributes.OFFSHORE_ATTR_DESCRIPTION);
					String addressValue = rs.getString(TableAttributes.OFFSHORE_ATTR_ADDRESS);
//					System.out.println(uniqueIDValue + " ||| " + descriptionValue + " ||| " + addressValue);
					
					double scoreForName = scorerForName.score(replaceKnownWords(gleiEntity.getLegalName()), 
															  replaceKnownWords(descriptionValue));
					
					double scoreForAddr = 1.0;
					if (matchAddress == true) {
						scoreForAddr = scorerForAddr.score(gleiEntity.getAddress(), addressValue);
					}				
					
					if (scoreForName >= similarityThresholdForName && scoreForAddr >= similarityThresholdForAddr) {
						OffshoreEntity entity = new OffshoreEntity(uniqueIDValue, descriptionValue, addressValue);
						
						entity.setSimilarityScoreForName(scoreForName);
						
						System.out.println("Name similarity Score between " + gleiEntity.getLegalName() 
								+ " and " + entity.getDescription() + ": " + scoreForName);

						if (matchAddress == true) {
							System.out.println("Address similarity Score between " + gleiEntity.getAddress() 
									+ " and " + entity.getAddress() + ": " + scoreForAddr);
						}
						
						offshoreEntities.add(entity);
					}
				}
				
				if (offshoreEntities.size() > 0) {
					gleiEntity.setMatchedOffshoreEntities(offshoreEntities);;
					System.out.println("Number of matched offshore entities: " + offshoreEntities.size() + " for GLEI entity " + gleiEntity.getLegalName());
					matchedGLEIEntities.add(gleiEntity);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void setGLEIEntities() {
		String query = queryMaker.getAllGLEICountryEntities();
		ResultSet rs = connector.executeQuery(query);
		try {
			while (rs.next()) {
				String LEIValue = rs.getString(TableAttributes.GLEI_ATTR_LEI);
				String legalNameValue = rs.getString(TableAttributes.GLEI_ATTR_LEGAL_NAME);
				String cityValue = rs.getString(TableAttributes.GLEI_ATTR_CITY);
				String addressValue = rs.getString(TableAttributes.GLEI_ATTR_ADDRESS);
//				System.out.println(LEIValue + " ||| " + legalNameValue + " ||| " + cityValue + " ||| " + addressValue);

				GLEIEntity entity = new GLEIEntity(LEIValue, legalNameValue, cityValue, addressValue);
				gleiEntities.add(entity);
			}

			System.out.println("Size of GLEI entities = " + gleiEntities.size());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private String replaceKnownWords(String input) {
		String output = input;
		
		String pattern = "\\b(?i:Ltd)\\b(\\.)?";
		output = output.replaceAll(pattern, "Ltd");
		pattern = "\\b(?i:Limited)\\b(\\.)?";
		output = output.replaceAll(pattern, "Ltd");
		pattern = "\\b(?i:Inc)\\b(\\.)?";
		output = output.replaceAll(pattern, "Inc");
		pattern = "\\b(?i:Co)\\b(\\.)?";
		output = output.replaceAll(pattern, "Co");
		pattern = "\\b(?i:N)\\b(\\.)A(\\.)";
		output = output.replaceAll(pattern, "National Association");
		
		return output;
	}

	public List<GLEIEntity> getMatchedGLEIEntities() {
		return matchedGLEIEntities;
	}

	public void closeConnector() {
		connector.closeConnector();
	}
	
	public void generateTextFileForMatchedEntities(String filePath) {
		String text = "GLEI_LEI " + "GLEI_LegalName " + "Offshore_UniqueID " + "Offshore_Description " + "SimilarityScore\n";
		for (GLEIEntity entity : matchedGLEIEntities) {
			for (OffshoreEntity offshoreEntity : entity.getMatchedOffshoreEntities()) {
				text += "\"" + entity.getLEI() + "\" " + "\"" + entity.getLegalName() + "\" " +
						"\"" + offshoreEntity.getUniqueID() + "\" " + "\"" + offshoreEntity.getDescription() + "\" " +
						"\"" + offshoreEntity.getSimilarityScoreForName() + "\"" + "\n";
			}
		}
		
		createFile(filePath, text);
	}
	
	private void createFile(String filePath, String text) {
		PrintWriter file = null;
		try {
			file = new PrintWriter(filePath);
			file.write(text);
			System.out.println("Created text file: " + filePath);
		} catch (FileNotFoundException e) {
			System.err.println("Error creating file: " + filePath);
		} finally {
			if (file != null) {
				file.close();
			}
		}
	}
}
