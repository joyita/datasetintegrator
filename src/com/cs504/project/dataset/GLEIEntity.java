package com.cs504.project.dataset;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class GLEIEntity {
	private String LEI;
	private String legalName;
	private String city;
	private String address;
	private List<OffshoreEntity> matchedOffshoreEntities;
	
	public GLEIEntity(String LEI, String legalName, String city, String address) {
		this.LEI = LEI;
		this.legalName = legalName;
		this.city = city;
		this.address = address;
		this.matchedOffshoreEntities = new ArrayList<OffshoreEntity>();
	}
	
	public String getLEI() {
		return LEI;
	}
	
	public void setLEI(String lEI) {
		LEI = lEI;
	}
	
	public String getLegalName() {
		return legalName;
	}
	
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}

	public List<OffshoreEntity> getMatchedOffshoreEntities() {
		return matchedOffshoreEntities;
	}

	public void setMatchedOffshoreEntities(
			List<OffshoreEntity> matchedOffshoreEntities) {
		this.matchedOffshoreEntities = matchedOffshoreEntities;
	}
	
	public String getFirstWordOfLegalName() {
		String firstWord = null;
		
		StringTokenizer tokenizer = new StringTokenizer(this.legalName, " ");
		firstWord = tokenizer.nextToken();
		
		if (firstWord.equalsIgnoreCase("The")) {
			firstWord += " " + tokenizer.nextToken(" ");
		}
		
		return firstWord;
	}
}
