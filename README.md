This program is about integrating two datasets: [GLEI dataset](https://dl.dropboxusercontent.com/u/54562279/Final.csv) and [Offshore entity dataset](http://offshoreleaks.icij.org/about/download).

Before running this program you need to setup a database with the above mentioned datasets. From the Offshore entity dataset I am using two tables: nodesNW.csv and node_countriesNW.csv.

In your database create the tables using the column names from the csv files. To insert data directly from the csv file I have used the following SQL query, where *filepath* is the absolute path of the csv file, *tablename* is where you want to insert the data from the csv file and *delimiter* would be either ',' for GLEI dataset and ';' for the offshore dataset.

```
#!SQL

LOAD DATA LOCAL INFILE 'filepath' INTO TABLE tablename
  FIELDS TERMINATED BY 'delimiter' ENCLOSED BY '"'
  LINES TERMINATED BY '\n'
  IGNORE 1 LINES;
```
Next thing you need to do is create two views from this tables. One is for all the GLEI data for a specific country. I used United States, you can use any country you want. I have used the following query for this:

```
#!SQL

CREATE 
VIEW gleius AS
    SELECT * FROM gleidata
    WHERE (gleidata.Country = 'United States')
```
The other view is for selecting only the Entity subtypes from the Offshore nodesNW table, since this table also contains people names and addresses which we don't need. I also joined the node_countriesNW with this table to have the country information, but it was not necessary. I have used the following query for this:

```
#!SQL

CREATE 
VIEW offshoreentity AS
    SELECT * FROM (offshorenodes JOIN offshorenodescountry)
    WHERE
        ((offshorenodes.UniqueID = offshorenodescountry.NodeID1)
            AND (offshorenodes.Subtypes = 'ENTITY'))
```
To use the above queries you might need to change the table and column names based on whatever you have used during creating the tables. You also need to change the column names in the *TableAttributes* class, if they are different from what I have used.

To run the program you might need to change values for some variable in the EntityMatcherTestRunner class. The code you need to change is given below:

```
#!java

package com.cs504.project.testrunners;

...

public class EntityMatcherTestRunner {
    public static void main(String args[]) {
        String url = "*database url*";
        String user = "*database username*";
        String password = "*database password for the user*";
        String gleiCountryTable = "*the view name created for a country from the GLEI dataset*";
        String offshoreEntityTable = "the view name created with the Entity subtypes from the Offshore dataset";

...
             entityMatcher.match(*similarityScoringTypeForName*, *similarityThresholdForName*);
...
             entityMatcher.generateTextFileForMatchedEntities("*filepath to write the matched entities*");

...

    }
}
```
There are five types of similarity metrics that are provided with this code, you can use any type from the ScoringType class and the threshold (between 0 to 1) you want to use for that metric to match the names from the two datasets and pass those in the *match* method of the *EntityMatcher* instance. 

```
#!java

public void match (String similarityScoringTypeForName, double similarityThresholdForName) 
```

There is another *match* method that takes four parameters, the first two is for matching names and the other two is for a similarity metric type and threshold to match the addresses from the two datasets.

```
#!java

public void match(String similarityScoringTypeForName, double similarityThresholdForName,
			     String similarityScoringTypeForAddr, double similarityThresholdForAddr) 
```

I have used the [SecondString Library](http://secondstring.sourceforge.net/) for similarity metrics. I have used a factory pattern to create objects for different similarity scoring mechanisms, if you want to add your own similarity scoring mechanism, just implement the *SimilarityScorer* interface and override the *score* method and then add it in the *SimilarityScorerFactory* class.

The external jar files needed to run the program are provided in the libs folder. I have used Eclipse to create the project. After cloning the project if you follow whatever I said above, you should be able to run it in Eclipse.

If you don't want to use Eclipse, you can run it from command line as well using the following commands, I used the commands in a Unix machine:

```
#!shell

# Get into your project folder first
cd /Path/to/DatasetIntegrator

# Put all the java file names in a text file
find . -name "*.java" > source.txt

# Compile the files
javac -cp .:libs/commons-lang3-3.3.2.jar:libs/log4j-1.2.17.jar:libs/mysql-connector-java-5.0.8-bin.jar:libs/secondstring-20120620.jar @source.txt

# Run the EntityMatcherTestRunner
nohup java -cp .:libs/commons-lang3-3.3.2.jar:libs/log4j-1.2.17.jar:libs/mysql-connector-java-5.0.8-bin.jar:libs/secondstring-20120620.jar:src com.cs504.project.testrunners.EntityMatcherTestRunner &
```
I have put my output files created by EntityMatcherTestRunner in the output folder. I have also created a simple user interface to check the output created by EntityMatcherTestRunner, where you can manually check the entity names from both datasets and select the ones you think actually matches and save them in a new text file. To see the user interface you have to run the *DatasetIntegratorUITestRunner* class and provide the input and output file path.